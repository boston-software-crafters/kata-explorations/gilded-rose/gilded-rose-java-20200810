package boston.codingdojo;

class GildedRose {

	Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (Item item : items) {
            if (item.name.equals("Aged Brie")) {
                increaseItemQualityUpToCap(item);

                advanceTheClock(item);

                if (item.sellInNDays < 0) {
                    increaseItemQualityUpToCap(item);
                }
                continue;
            }

            if (item.name.equals("Backstage passes to a TAFKAL80ETC concert")) {
                if (item.quality < 50) {
                    item.quality = item.quality + 1;

                    if (item.sellInNDays < 11) {
                        increaseItemQualityUpToCap(item);
                    }

                    if (item.sellInNDays < 6) {
                        increaseItemQualityUpToCap(item);
                    }
                }

                advanceTheClock(item);

                if (item.sellInNDays < 0) {
                    item.quality = 0;
                }
                continue;
            }

            if (item.name.equals("Sulfuras, Hand of Ragnaros")) {
                continue;
            }

            /* if (default case) */ {
                decreaseItemQualityDownToFloor(item);

                advanceTheClock(item);

                if (item.sellInNDays < 0) {
                    decreaseItemQualityDownToFloor(item);
                }
            }
        }
    }

    private void advanceTheClock(Item item) {
        item.sellInNDays = item.sellInNDays - 1;
    }

    private void decreaseItemQualityDownToFloor(Item item) {
        if (item.quality > 0) {
            item.quality = item.quality - 1;
        }
    }

    private void increaseItemQualityUpToCap(Item item) {
        if (item.quality < 50) {
            item.quality = item.quality + 1;
        }
    }
}