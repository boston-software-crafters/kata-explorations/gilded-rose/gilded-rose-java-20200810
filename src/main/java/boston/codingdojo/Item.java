package boston.codingdojo;

public class Item {

    public String name;

    public int sellInNDays;

    public int quality;

    public Item(String name, int sellIn, int quality) {
        this.name = name;
        this.sellInNDays = sellIn;
        this.quality = quality;
    }

    @Override
    public String toString() {
        return this.name + ", " + this.sellInNDays + ", " + this.quality;
    }
}