package boston.codingdojo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class ParameterizedItemTest {

	@Parameters(name="{index}: {0}")
	public static Iterable<Object[]> data() {
		return(Arrays.asList(new Object[][] {
			// testLabel, product, initial daysToExpire, initial quality, expected daysToExpire, expected quality
                {"Normal item degrades when not expired", "+5 Dexterity Vest", 5, 4, 7, 6},
                {"Normal item quality does not degrade when quality zero", "+5 Dexterity Vest", 4, 3, 0, 0},
                {"Normal item degrades double when expire = 0", "+5 Dexterity Vest", 0, -1, 7, 5},
                {"Normal item degrades double when expire < 0", "Elixir of the Mongoose", -1, -2, 7, 5},
                {"Sulfuras quality does not change", "Sulfuras, Hand of Ragnaros", 3, 3, 80, 80},
                {"Sulfuras quality does not change when expire", "Sulfuras, Hand of Ragnaros", -1, -1, 80, 80},
                {"AgedBrie quality caps at 50", "Aged Brie", 2, 1, 50, 50},
                {"AgedBrie quality caps at 50 when expire", "Aged Brie", 0, -1, 50, 50},
                {"AgedBrie quality caps at 50 when expire", "Aged Brie", -1, -2, 50, 50},
                {"AgedBrie quality caps at 50 when expire", "Aged Brie", 0, -1, 49, 50},
                {"AgedBrie quality +1", "Aged Brie", 5, 4, 7, 8},
                {"AgedBrie quality +1", "Aged Brie", 1, 0, 7, 8},
                {"AgedBrie quality +2 when expire", "Aged Brie", 0, -1, 41, 43},
                {"AgedBrie quality +2 when expire", "Aged Brie", -2, -3, 46, 48},

        }));
	}

	private final String product;
	private final int initialDaysToExpire;
	private final int initialQuality;
	private final int expectedDaysToExpire;
	private final int expectedQuality;

	public ParameterizedItemTest(
			@SuppressWarnings("unused") String testDescription,
			String product,
			int initialDaysToExpire,
			int expectedDaysToExpire,
			int initialQuality,
			int expectedQuality )
	{
		this.product = product;
		this.initialDaysToExpire = initialDaysToExpire;
		this.initialQuality = initialQuality;
		this.expectedDaysToExpire = expectedDaysToExpire;
		this.expectedQuality = expectedQuality;
	}

	@Test
	public void parameterizedItemTest() {
		Item[] items = {new Item(product,initialDaysToExpire,initialQuality)};
		GildedRose inn = new GildedRose(items);
		Item item = items[0];
		inn.updateQuality();
		assertEquals(expectedQuality, item.quality);
		assertEquals(expectedDaysToExpire, item.sellInNDays);
	}
}
