package boston.codingdojo;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AgedBrieTest {

	private GildedRose inn;

	@Test
	public void testAgedBrieIncrements() {
		int daysToExpire = 5;
		int quality = 7;
		int expectedQuality = 8;
		Item item = createInnWithStandardAgedBrie(daysToExpire, quality);
		inn.updateQuality();
		assertEquals(expectedQuality, item.quality);
	}


	@Test
	public void testAgedBrieDaysToExpireDecrements() {
		int DEFAULT_DELTA_SELL_IN = -1;

		int daysToExpire = 5;
		int quality = 7;
		int expectedDaysToExpire = daysToExpire + DEFAULT_DELTA_SELL_IN;
		Item item = createInnWithStandardAgedBrie(daysToExpire, quality);
		inn.updateQuality();
		assertEquals(expectedDaysToExpire, item.sellInNDays);
	}
	
	@Test
	public void testAgedBrieQualityCapsAt50() {
		int daysToExpire = 5;
		int MAX_STANDARD_QUALITY = 50;
		int expectedQuality = MAX_STANDARD_QUALITY;
		Item item = createInnWithStandardAgedBrie(daysToExpire, expectedQuality);
		inn.updateQuality();
		assertEquals(expectedQuality, item.quality);
	}
	
	@Test
	public void testAgedBrieQualityStableAfterDaysToExpireAt50() {
		int SELL_IN_EXPIRATION = 0;
		int MAX_STANDARD_QUALITY = 50;

		int daysToExpire = SELL_IN_EXPIRATION;
		int quality = MAX_STANDARD_QUALITY;
		Item item = createInnWithStandardAgedBrie(daysToExpire, quality);
		inn.updateQuality();
		assertEquals(quality, item.quality);
	}
	
	private Item createInnWithStandardAgedBrie(int daysToExpire, int quality) {
		Item[] items = {new Item("Aged Brie",daysToExpire,quality)};
		inn = new GildedRose(items);
		return items[0];
	}

	@Test
	public void testAgedBrieQualityIncreasesByTwoAfterDaysToExpireUnder50() {
		int SELL_IN_EXPIRATION = 0;
		int AGED_BRIE_EXPIRED_DELTA_QUALITY = 2;

		int daysToExpire = SELL_IN_EXPIRATION;
		int quality = 46;
		int expectedQuality = quality + AGED_BRIE_EXPIRED_DELTA_QUALITY;
		Item item = createInnWithStandardAgedBrie(daysToExpire, quality);
		inn.updateQuality();
		assertEquals(expectedQuality, item.quality);
	}
}
