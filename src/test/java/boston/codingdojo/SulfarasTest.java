package boston.codingdojo;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SulfarasTest {
	@Test
	public void testQualityOfSulfuras() {
		Item sulfuras1 = new Item("Sulfuras, Hand of Ragnaros", 3, 80);
		GildedRose inn = new GildedRose(new Item[] {sulfuras1});
		inn.updateQuality();
		assertEquals(80,sulfuras1.quality);
	}

	@Test
	public void testDaysToExpireOfSulfuras() {
		Item sulfuras1 = new Item("Sulfuras, Hand of Ragnaros", 3, 80);
		GildedRose inn = new GildedRose(new Item[] {sulfuras1});
		inn.updateQuality();
		assertEquals(3,sulfuras1.sellInNDays);
	}

	@Test
	public void testSulfurasDoesntExpire() {
		Item sulfuras1 = new Item("Sulfuras, Hand of Ragnaros", -1, 80);
		GildedRose inn = new GildedRose(new Item[] {sulfuras1});
		inn.updateQuality();
		assertEquals(80,sulfuras1.quality);
		assertEquals(-1,sulfuras1.sellInNDays);
	}

}
